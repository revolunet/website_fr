#!/usr/bin/env bash

echo "Récupération des modifications…"
git stash
git pull origin master

echo "Installation de Pelican"
source bin/activate

echo "Prégénération de la licence dans le wiki"
python replace.py

echo "Génération du site"
pelican -s publishconf.py --debug

echo "Copie des fichiers…"
mkdir -p ../fr
cp -R output/* ../fr/

echo "Génération de duniter.org/fr terminée."
